# Dynamic menu item

Allows to create custom menu item and then assign nodes
to it via node editing interface.

## Configuration

After installing module head to `/admin/structure/menu/dynamic_menu_item` 
and create menu.
Edit user permission `Edit dynamic menu item` to allow users to see menu
when they are editing any node.
Once use tick the box and save the node, the menu link will change to
the current node.
